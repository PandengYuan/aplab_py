package simple;

import java.util.*;

class SetElement implements Comparable<SetElement> {
	int key;	int freq;

	SetElement(int key) {
		this.key = key;
		freq = 0;
	}

	void update() { freq++; }
	public int compareTo(SetElement that)  {
		return -1*Integer.compare(freq, that.freq);
	}
	public String toString() { return String.format("<%s,%d>", key, freq); }
}


class MultiSet {

	Map<Integer, SetElement> map;

	MultiSet(ArrayList<Integer> gaps) {
		map = new HashMap<>();
		for (int element : gaps) {
			SetElement x = map.get(element);
			if (x==null) {
				x = new SetElement(element);
				map.put(x.key, x); // push only once
			}
			x.update(); // increment freq
		}
	}

	public String toString() {
		//List<SetElement> values = new ArrayList<>(map.values());
		String s = "";
		for (SetElement element : new ArrayList<SetElement>(map.values())) {
			s += element.key + "\t" + element.freq + "\n";
		}
		return s;
	}

}


class HistogramOfPrimeGapValues {

	//the method for judging whether an Integer is a prime number. if so, add it into set.
	public static void erastothenesSieve(Set<Integer> primes, int n){
		for (int x : primes) {
			if (n%x == 0 )  return;
		}
		primes.add(n);
	}

	//the method for finding all the prime numbers that satisfy the condition
	public static Set<Integer> findPrimes(int range){
		Set<Integer> primes = new TreeSet<>();
		primes.add(2);
		for (int i = 3; i < range; i++) {
			erastothenesSieve(primes, i);
		}
		return primes;
	}

	//calculate the gaps and add them into arraylist
	public static ArrayList<Integer> findGaps(Set<Integer> primesSet){
		ArrayList<Integer> gaps = new ArrayList<Integer>();
		Integer[] primes = primesSet.toArray(new Integer[0]);
		for (int i = 0; i < primes.length; i++) {
			for (int j = i+1; j < primes.length; j++) {
				gaps.add(primes[j]-primes[i]);
			}
		}
		return gaps;
	}


	public static void main(String[] args) {

		if (args.length < 1) {
			System.out.println("usage: java PrimeFinder <max-range>");
			return;
		}

		//call methods and print result
		System.out.println(new MultiSet(findGaps(findPrimes(Integer.parseInt(args[0])))));
	}
}
