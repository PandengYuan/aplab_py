package simple;

class GenericPatternDemo {
	// in: <'*', 3> out: "***"
	static String repeatChar(char ch, int n) {
		StringBuilder buff = new StringBuilder();
		for (int i=0; i < n; i++)
			buff.append(ch);
		return buff.toString();
	}

	// in: <'(', 2, ')', 2, *, 6> out: "((**))"
	static String genLine(char left, int nleft, char right, int nright, char middle, int width) {
		int nmiddle =  width - (nleft + nright);
		if (nmiddle < 0 || nmiddle > width)
			return null;

		String left_str = repeatChar(left, nleft);
		String right_str = repeatChar(right, nright);
		String mid_blank = repeatChar(middle, nmiddle);

		return left_str + mid_blank + right_str; 
	}

	//* * " " 20
	//" " " " * 20
	//| | * 20
	public static void main(String[] args) {

		if (args.length < 4) {
			System.err.println("usage java GenericPatternDemo <left_char> <right_char> <middle_char> <width>");
			return;
		}

		//processing arguments
		final int N = Integer.parseInt(args[3]);
		final char LEFT_PIXEL = args[0].charAt(0);
		final char RIGHT_PIXEL = args[1].charAt(0);
		final char MIDDLE_PIXEL = args[2].charAt(0);

		String line;

		//The top half of the graph
		int i = 1;
		while ((line =  genLine(LEFT_PIXEL, i, RIGHT_PIXEL, i, MIDDLE_PIXEL, N)) != null) {
			System.out.println(line);
			i++;
		}

		//The bottom half of the graph
		i = N/2;
		while ((line =  genLine(LEFT_PIXEL, i, RIGHT_PIXEL, i,MIDDLE_PIXEL, N)) != null) {
			System.out.println(line);
			i--;
		}

	}
}
