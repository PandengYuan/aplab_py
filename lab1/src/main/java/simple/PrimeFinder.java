package simple;

import java.util.ArrayList;

class PrimeFinder {

	static boolean isPrime(int n) {
		for (int x=2; x<=(int)Math.sqrt(n); x++) {
			if (n%x == 0) {
				return false; // early termination
			}
		}
		return true; // not divisible by any number <= sqrt(n)
	}

	//specify only a maximum value to restrict search
	// 20
	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("usage: java PrimeFinder <max-range>");
			return;
		}

		int maxRange = Integer.parseInt(args[0]);
		for (int i=2; i<= maxRange; i++) {
			if (isPrime(i)) {
				System.out.println(i);
			}
		}
	}


//	//specify the minimum and the maximum of prime number search
//	//100 1000
//	public static void main(String[] args) {
//		if (args.length < 2) {
//			System.out.println("usage: java PrimeFinder <min-range> <max-range>");
//			return;
//		}
//
//		int minRange = Integer.parseInt(args[0]);
//		int maxRange = Integer.parseInt(args[1]);
//		for (int i=minRange; i<= maxRange; i++) {
//			if (isPrime(i)) {
//				System.out.println(i);
//			}
//		}
//	}


	//Check whether the difference between two numbers is equal to the given number
	static String checkDifference(int p1, int p2, int num){
		if (p2 - p1 == num) {
			return "("+p1+","+p2+")";
		}
		return null;
	}

	//Find out the pairs of prime numbers that match the given difference
	static String findOut_SpecificPrimes(ArrayList<Integer> arrayList, int num ){
		String string = "";
		for (int i = 0; i < arrayList.size(); i++) {
			for (int j = i+1; j < arrayList.size(); j++) {
				if (checkDifference(arrayList.get(i), arrayList.get(j), num) != null) {
					string += checkDifference(arrayList.get(i), arrayList.get(j), num)+" ";
				}
			}
		}
		return  string;
	}


//	//find twin, cousin and sexy primes within a given maximum integer bound
//	//20
//	public static void main(String[] args) {
//		if (args.length < 1) {
//			System.out.println("usage: java PrimeFinder <max-range>");
//			return;
//		}
//
//		//Find all the prime numbers that satisfy the condition and add them to the arraylist
//		ArrayList<Integer> arrayList = new ArrayList<Integer>();
//		int maxRange = Integer.parseInt(args[0]);
//		for (int i=2; i<= maxRange; i++) {
//			if (isPrime(i)) {
//				arrayList.add(i);
//			}
//		}
//
//		//call findOut_SpecificPrimes method and print results
//		System.out.println("twin primes:"+findOut_SpecificPrimes(arrayList,2));
//		System.out.println("cousin primes:"+findOut_SpecificPrimes(arrayList,4));
//		System.out.println("sexy primes:"+findOut_SpecificPrimes(arrayList,6));
//	}



}
		
