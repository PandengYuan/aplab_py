package _4gons;

import shapes.Vec2d;

import java.util.ArrayList;

public class Q9_Test {

    //area calculation test
    public static void main(String[] args) {

        //create an arraylist
        ArrayList<_4gon> _4gons = new ArrayList<_4gon>();

        //create different knids of objects and add them into the arraylist
        _4gons.add(new Parallelogram(new Vec2d(0,0), new Vec2d(4,0), new Vec2d(5,1), new Vec2d(1,1)));
        _4gons.add(new Rectangle(new Vec2d(0,0), new Vec2d(4,0), new Vec2d(4,1), new Vec2d(0,1)));
        _4gons.add(new Square(new Vec2d(0,0), new Vec2d(1,0), new Vec2d(1,1), new Vec2d(0,1)));

        //foreach and call area method
        for (_4gon element : _4gons) {
            System.out.println(element.area());
        }

    }

}
