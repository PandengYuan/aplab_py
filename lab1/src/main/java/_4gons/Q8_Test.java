package _4gons;

import shapes.Vec2d;

import java.util.ArrayList;

public class Q8_Test {

    //convexity check test
    public static void main(String[] args) {

        //non-convex
        System.out.println(new _4gon(new Vec2d(0,0), new Vec2d(-10,-10), new Vec2d(0,10), new Vec2d(10,-10)).isConvex());
        //convex
        System.out.println(new _4gon(new Vec2d(0,10), new Vec2d(0,0), new Vec2d(30,0), new Vec2d(10,20)).isConvex());


    }

}
