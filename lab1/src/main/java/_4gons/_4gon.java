package _4gons;

import shapes.Vec2d;

class _4gon {
	Vec2d a, b, c, d;

	_4gon(Vec2d a, Vec2d b, Vec2d c, Vec2d d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	float perimeter() {
		return a.minus(b).l2norm() + b.minus(c).l2norm() +
			c.minus(d).l2norm() + d.minus(a).l2norm();
	}

	//the method for calculating the area of triangle through its three vertex coordinates (Heron's formula)
	float triangleArea(Vec2d v1, Vec2d v2, Vec2d v3){
		float p = (v1.minus(v2).l2norm() + v2.minus(v3).l2norm() + v3.minus(v1).l2norm())/2;
		return (float) Math.sqrt(p*(p-v1.minus(v2).l2norm())*(p-v2.minus(v3).l2norm())*(p-v3.minus(v1).l2norm()));
	}

	//judge whether this quadrilateral is convex
	boolean isConvex(){
		if (Math.round((triangleArea(a,b,c) + triangleArea(a,c,d))*1000) == Math.round((triangleArea(b,c,d) + triangleArea(a,b,d))*1000)) {
			return true;
		}
		return false;
	}

	//the method used to compute the area of any quadrilateral
	float area(){
		if (this.isConvex())
			return triangleArea(a,b,c) + triangleArea(a,c,d);
		else{
			if ((triangleArea(a,b,c) + triangleArea(a,c,d)) <= (triangleArea(b,c,d) + triangleArea(a,b,d)))
				return triangleArea(a,b,c) + triangleArea(a,c,d);
			else
				return triangleArea(b,c,d) + triangleArea(a,b,d);
		}
	}

}


interface ValidityChecker {
	boolean isValid();
}


class Parallelogram extends _4gon implements ValidityChecker {

	Parallelogram(Vec2d a, Vec2d b, Vec2d c, Vec2d d) {
		super(a, b, c, d);  // reuse the constructor function
	}

	public boolean isValid() {
		//TODO: Insert logic for checking if this is a valid parallelogram
		//ab == dc && ad == bc
		if (a.minus(b).l2norm() == d.minus(c).l2norm() && a.minus(d).l2norm() ==b.minus(c).l2norm()) {
			return true;
		}
		return false;
	}

}

class Rectangle extends Parallelogram {
	float l_a, l_b;

	Rectangle(Vec2d a, Vec2d b, Vec2d c, Vec2d d) {
		super(a, b, c, d);  // reuse the constructor function
		l_a = a.minus(b).l2norm();
		l_b = b.minus(c).l2norm();
	}

	public boolean isValid() {
		//TODO: Insert logic for checking if this is a valid rectangle
		//use cosine law to derive that the cosine of any corner is 0
		if (super.isValid()){
			if (((a.getX() - b.getX()) * (c.getX() - b.getX()) + (a.getY() - b.getY()) * (c.getY() - b.getY())) / (l_a * l_b) == 0) {
				return true;
			}
		}
		return false;
	}
}

class Square extends Rectangle {

	Square(Vec2d a, Vec2d b, Vec2d c, Vec2d d) {
		super(a, b, c, d);  // reuse the constructor function
	}

}
